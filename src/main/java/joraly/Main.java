package joraly;


import java.util.Locale;
import java.util.Scanner;

public class Main {
    public static final double RADIUS = 6371.0  ;
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in).useLocale(Locale.ENGLISH);
        System.out.print("Введите широту первой точки: ");
        var lat1 = scanner.nextDouble();
        System.out.print("Введите долготу первой точки: ");
        var lon1 = scanner.nextDouble();
        System.out.print("Введите широту второй точки: ");
        var lat2 = scanner.nextDouble();
        System.out.print("Введите долготу второй точки: ");
        var lon2 = scanner.nextDouble();
        System.out.println("Расстояние: " + distance(lat1, lon1, lat2, lon2));
    }
    private static double distance(Double lat1, Double lon1, Double lat2, Double lon2) {
        //2 * R * arcsin(√(sin²((lat₂ - lat₁)/2) + cos(lat₁) * cos(lat₂) * sin²((lon₂ - lon₁)/2)))
        double latDistance = Math.toRadians(lat2 - lat1);
        double lonDistance = Math.toRadians(lon2 - lon1);
        double a = Math.sin(latDistance / 2) * Math.sin(latDistance / 2)
                + Math.cos(Math.toRadians(lat1)) * Math.cos(Math.toRadians(lat2))
                * Math.sin(lonDistance / 2) * Math.sin(lonDistance / 2);

        double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));

        double distance = RADIUS * c;

        return distance;
    }
}